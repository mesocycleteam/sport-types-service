package com.mesocycle.sporttypesservice.controller;

import com.mesocycle.sporttypesservice.domain.SportType;
import com.mesocycle.sporttypesservice.repository.SportTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SportTypeController {

    private static final Logger LOG = LoggerFactory.getLogger(SportTypeController.class);


    @Autowired
    private SportTypeRepository sportTypeRepository;

    @GetMapping("/sport-types/all")
    public Iterable<SportType> fetchAllSportTypes() {
        LOG.info("Accessing Sport Types");
        return sportTypeRepository.findAll();
    }
}
