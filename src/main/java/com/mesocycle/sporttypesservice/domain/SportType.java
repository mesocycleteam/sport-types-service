package com.mesocycle.sporttypesservice.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class SportType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long sportTypeId;

    private String name;
}
