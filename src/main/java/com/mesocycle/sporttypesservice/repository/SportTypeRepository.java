package com.mesocycle.sporttypesservice.repository;

import com.mesocycle.sporttypesservice.domain.SportType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface SportTypeRepository extends CrudRepository<SportType, Long> {
}
